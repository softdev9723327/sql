/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.model.User;
import com.mycompany.service.UserService;

/**
 *
 * @author asus
 */
public class TestUserService {

    public static User main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1", "password");
        if (user != null ) {
            System.out.println("Welcome user :"+user.getName());
        }else{
            System.out.println("Error");
        }
        return null;
        
    }
}
